SHOW DATABASES;

-- Create the blog_db database
CREATE DATABASE blog_db;

-- Switch to the blog_db database
USE blog_db;

-- Create the users table
CREATE TABLE users (
  id INT NOT NULL AUTO_INCREMENT,
  email VARCHAR(100) NOT NULL,
  password VARCHAR(300) NOT NULL,
  datetime_created DATETIME NOT NULL,
  PRIMARY KEY(id)
);

-- Create the posts table
CREATE TABLE posts (
  id INT NOT NULL AUTO_INCREMENT,
  author_id INT NOT NULL,
  title VARCHAR(500) NOT NULL,
  content VARCHAR(500) NOT NULL,
  datetime_posted DATETIME NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (author_id) REFERENCES users(id)
);

-- Create the post_comments table
CREATE TABLE post_comments (
  id INT NOT NULL AUTO_INCREMENT,
  post_id INT NOT NULL,
  user_id INT NOT NULL,
  content VARCHAR(500) NOT NULL,
  datetime_commented DATETIME NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (post_id) REFERENCES posts(id),
  FOREIGN KEY (user_id) REFERENCES users(id)
);

-- Create the post_likes table
CREATE TABLE post_likes (
  id INT NOT NULL AUTO_INCREMENT,
  post_id INT NOT NULL,
  user_id INT NOT NULL,
  datetime_liked DATETIME NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (post_id) REFERENCES posts(id),
  FOREIGN KEY (user_id) REFERENCES users(id)
);

--- users
INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-1-1 1:00:00");

INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", passwordB, "2021-1-1 2:00:00");

INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", passwordC, "2021-1-1 3:00:00");

INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", passwordD, "2021-1-1 4:00:00");

INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", passwordE, "2021-1-1 5:00:00");


-- posts
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World", "2021-1-2 1:00:00");

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth", "2021-1-2 2:00:00");

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "Third Code", "Welcome to Mars", "2021-1-2 3:00:00");

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "Fourth Code", "Bye bye solar system", "2021-1-2 4:00:00");



SELECT title FROM posts WHERE author_id = 1;

SELECT email, datetime_created FROM users;

UPDATE posts SET content = "Hello to the people of the Earth! WHERE title = "Second Code";


DELETE FROM users WHERE email = "johndoe@gmail.com";